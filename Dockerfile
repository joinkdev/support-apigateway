FROM java:8

ENV PORT 8080

ENV LIFECYCLE development
ENV BUILD_HOME /build
ENV APP_HOME /app
ENV GRADLE_HOME /usr/bin/gradle-2.6
ENV PATH $PATH:$GRADLE_HOME/bin
ENV TERM=${TERM:-dumb}

EXPOSE 8080

WORKDIR /usr/bin
RUN wget -q https://services.gradle.org/distributions/gradle-2.6-bin.zip -O gradle.zip \
    && unzip -q gradle.zip \
    && rm gradle.zip \
    && mkdir $BUILD_HOME \
    && mkdir $APP_HOME

COPY . $BUILD_HOME
WORKDIR $BUILD_HOME

RUN gradle build \
	&& mv $(find ${BUILD_HOME}/build/libs/ -name \*.jar) $APP_HOME/app.jar \
	&& rm -rf $BUILD_HOME $GRADLE_HOME

CMD java -jar $APP_HOME/app.jar