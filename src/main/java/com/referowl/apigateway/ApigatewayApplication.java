package com.referowl.apigateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by ChrisCone on 9/13/15.
 */
@SpringBootApplication
@EnableCircuitBreaker
@EnableZuulProxy
@EnableDiscoveryClient
@ComponentScan
public class ApigatewayApplication {
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(ApigatewayApplication.class);
        app.setShowBanner(false);
        app.run(args);
    }
}
